from pathlib import Path
from urllib import request
from urllib.error import HTTPError

import toml
from pytest import fixture, raises

from pdm_gitlab_example import __version__


@fixture
def pyproject_version(scope="session"):
    t = toml.load((Path(__file__) / "../../pyproject.toml").resolve())
    return t["project"]["version"]


def test_version_consistency(pyproject_version):
    assert __version__ == pyproject_version, "version mismatch"


def test_pypi_collision():
    with raises(HTTPError) as exc:

        # remove "test." for uploading as official PyPi package
        request.urlopen(
            f"https://test.pypi.org/project/pdm-gitlab-example/{__version__}/"
        )
    assert "404" in str(exc.value), f"version {__version__} already uploaded to PyPi"
