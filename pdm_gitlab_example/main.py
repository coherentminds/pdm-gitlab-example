from devtools import debug  # type: ignore


def some_func(b: bool) -> int:
    if b:
        debug(b)
        return 42
    else:
        return 0
