import os

import nox  # type: ignore

os.environ.update({"PDM_IGNORE_SAVED_PYTHON": "1"})
nox.options.reuse_existing_virtualenvs = True


def runner(session, cmds, external=False):
    session.run(*cmds.split(), external=external)


@nox.session
@nox.parametrize("devtools", ["0.5.1", "0.6.1"])
def test(session, devtools):
    runner(session, "pdm install -s test", external=True)
    runner(session, f"pdm add devtools=={devtools}", external=True)
    runner(session, "pytest tests/")


@nox.session
def coverage(session):
    runner(session, "pdm install -s test", external=True)
    runner(session, "pytest --cov=pdm_gitlab_example tests/")
    runner(session, "coverage xml")


@nox.session
def lint(session):
    runner(session, "pdm install -s lint", external=True)
    runner(
        session,
        "mypy --disallow-untyped-defs --disallow-untyped-calls pdm_gitlab_example/",
    )
    runner(session, "black --check --diff pdm_gitlab_example/ tests/ noxfile.py")
    runner(session, "isort --check --diff pdm_gitlab_example tests noxfile.py")
    runner(session, "flake8 pdm_gitlab_example/ tests/ noxfile.py")
